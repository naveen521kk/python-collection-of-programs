"""Write a program to find and display the count of all those numbers which are between 1 and N,which are
either divisible by 3 or by 7. 2 For example :
If the value of N is 15
The sum should be displayed as
7"""
n=int(input())
count=0
for i in range(1,n+1):
    if i%3==0 or i%7==0:
        count+=1
print(count)
