"""Write a program to swap the first half of the content of a list Numbers with second half of the content of list
Numbers and display the swapped values. Note : Assuming that the list has even number of values in it. For
example :
If the list Numbers contains
[35,67,89,23,12,45]
After swapping the list content should be displayed as
[23,12,45,35,67,89]"""
lst=[35,67,89,23,12,45]
mid=len(lst)//2
nl=lst[mid:]+lst[:mid]
print(nl)
