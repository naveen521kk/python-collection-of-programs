"""Write definition of a method/function HowMany(ID,Val) to count and display number of times the value of Val
is present in the list ID. For example : If the ID contains [115,122,137,110,122,113] and Val contains 122
Output is 122 found 2 Times"""
def HowMany(ID,Val):
    count=0
    for i in ID:
        if i==Val:
            count+=1
    return count
print(HowMany([115,122,137,110,122,113],122))
