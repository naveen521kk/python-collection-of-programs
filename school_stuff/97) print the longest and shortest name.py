'''WAP to accept 10 names in a list and print the longest and shortest name without using sort().'''
l=eval(input('Enter 10 names'))
for i in range(len(l)):
    for j in range(i+1,len(l)):
        if len(l[i])>len(l[j]):
            l[i],l[j]=l[j],l[i]
print('Longest: ',l[-1])
print('Shortest: ',l[0])
