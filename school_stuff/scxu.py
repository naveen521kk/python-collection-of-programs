import PySimpleGUI as sg

sg.theme("DarkTanBlue")
right_click_menu_info = ["nice", ["nice try lol", "not allowed"]]
info_layout = [
    [
        sg.Text(
            "Done By Naveen521kk",
            justification="center",
            text_color="#fff",
            background_color="#000",
            right_click_menu=right_click_menu_info,
            size=(50,50)
        )
    ]
]
image_uploader = [[sg.Text("Upload Image to SCXU.net",justification="center",font=""]]

layout = image_uploader
window = sg.Window("SCXU", layout, resizable=True, finalize=True)
window.maximize()

while True:
    event, values = window.read()
    if event in (None, "Exit"):
        break
    elif event == "SUBMIT":
        pass
    elif event == "CLEAR FILLED":
        pass
    else:
        print(event)

window.close()