'''A function to read from exitisting text file lines.txt 
and display the words that start and end with vowel and has string length
 more than 5'''

def files_read(fp):
    with open(fp) as f:
        content = f.read()
        words = content.split()
        words = [i.strip() for i in words]
        for i in words:
            if (i[0] in 'aeiou' and i[-1] in 'aeiou') and len(i)>5:
                print(i)
fp = 'lines.txt.txt'
files_read(fp)