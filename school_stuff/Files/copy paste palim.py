'''write a function that takes argument of name of text file that has to be copied 
and display only the words that are palindrome'''

def text_function(fp):
    with open(fp) as f:
        content= f.read()
    words = content.split()
    for i in words:
        if i == i[::-1]:
            print(i)