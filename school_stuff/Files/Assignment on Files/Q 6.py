def files():
    with open('movie.txt') as f:
        content = f.read()
        words = content.split()
        maxLength = 0; maxLengthStr = ''
        minLength = len(words[0]); minLengthStr = ''
        for i in words:
            i = i.strip()
            if len(i) > maxLength and i.isspace() == False:
                maxLength = len(i)
                maxLengthStr = i
            elif len(i) < minLength and i.isspace() == False:
                minLength = len(i)
                minLengthStr = i
        return minLengthStr,maxLengthStr
print(files())



