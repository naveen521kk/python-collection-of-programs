'''To open a file called marina.txt, count and print the number
of lines, number of words, number of alphabets, uppercase letters,
lowercase letters, number of vowels, number of consonants, digits,
spaces and special characters'''

mainDict={
    "lines":0,
    "words":0,
    "alphac":0,
    "upperc":0,
    "lowerc":0,
    "vowelc":0,
    "consolantc":0,
    "digitc":0,
    "spacesc":0,
    "specialc":0
}

with open('marina.txt','r') as f:
    content = f.read()
    lines = content.split('\n')
    mainDict["lines"] = len(lines)
    mainDict["words"] = len(content)
    for i in content:
        if i.isspace():
            mainDict['spacesc']+=1
            if not i.isalnum(): #check not spaces and check num or alpha
                mainDict['specialc']+=1
        elif i.isalpha():
            mainDict['alphac']+=1
            if i.lower() in 'aeiou':
                mainDict['vowelc']+=1
            else:
                mainDict['consolantc']+=1
            if i.isupper():
                mainDict['upperc']+=1
            else:
                mainDict['lowerc']+=1
        elif i.isdigit():
            mainDict['digitc']+=1
for key in mainDict:
    print("Number of {}:{}".format(key,mainDict[key]))
