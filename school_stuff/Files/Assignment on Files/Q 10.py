'''Write a function to open a file
called city.txt, copy all consonant
words to a new file called 
mycity.txt and print the new file
content'''
def new_file_consolants():
    with open('city1.txt') as f:
        content = f.read()
        words = [i.strip() for i in content.split()]
        consoleList = []
        for i in words:
            for j in list('aeiouAEIOU'):
                if j in i:
                    break
            else:
                consoleList.append(i)
        with open('mycity.txt','w') as fw:
            [fw.write(i+' ') for i in consoleList]
        with open('mycity.txt','r') as fr:
            print("File Contents:",fr.read())
new_file_consolants()
