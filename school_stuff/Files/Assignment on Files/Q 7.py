'''Write a function to open a file called school.txt,
read the data,
print all 4 letter words in reverse and print all 
other words as-it-is.'''
def file_print_reverse():
    with open("school.txt") as f:
        content = f.read()
        words = content.split()
        for i in words:
            if len(i)==4:
                print(i[::-1],end=' ' )
            else:
                print(i,end=' ')

print(file_print_reverse())