def avg_word_size(fp):
    '''Returns average work size of a file path passed
    as `fp`'''
    with open(fp) as f:
        content = f.read()
        numchar = len(content)
        numwords = len(content.strip())
    return numchar/numwords

def avg_line_size(fp):
    '''Returns average line size of content of the file path
    `fp`'''
    with open(fp) as f:
        lines = f.readlines()
        content = ''
        for i in lines:
            content+=i
    numchar = len(content)
    numlines = len(lines)
    return numchar/numlines

def avg_words_per_line(fp):
    with open(fp) as f:
        lines = f.readlines()
        content = ''
        for i in lines:
            content+=i
        return len(content.split(),len(lines))
print(avg_word_size('skywalk.txt'))


