'''Write a function to open a file called myself.txt,
count and print the occurrences of
each word in the file.'''
def count_words():
    with open('myself.txt') as f:
        content = f.read()
        words = content.split()
    wordsDict = {}
    for i in words:
        i = i.lower()
        if i not in wordsDict:
            wordsDict[i] = 1
        else:
            wordsDict[i]+=1
    print(wordsDict)
    for word,count in zip(wordsDict.keys(),wordsDict.values()):
        print("Word: {}---{}".format(word,count))

count_words()