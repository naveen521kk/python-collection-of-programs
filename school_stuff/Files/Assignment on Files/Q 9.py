'''Write a function to open a file
called school.txt and print all words 
which are either starts or ends 
with any vowel characters.'''

def files_read(fp):
    with open(fp) as f:
        content = f.read()
        words = content.split()
        words = [i.strip() for i in words]
        for i in words:
            if i[0] in 'aeiou' or i[-1] in 'aeiou':
                print(i)
fp = 'school1.txt'
files_read(fp)