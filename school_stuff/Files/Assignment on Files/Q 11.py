'''Write a function to open a file 
called kite.txt, also accept a 
searching word, word to be replaced 
and search and replace all 
occurrences of the searching word. 
Count and print the number of 
replacements also.'''
def find_and_replace(wordtoSearch,wordtoReplace):
    count = 0
    with open('kite.txt') as f:
        content = f.read()
        returncontent = ''
        i = 0
        while i<=len(content)-len(wordtoSearch): #ignore last part
            if content[i] == wordtoSearch[0]:
                #print(returncontent)
                for j in range(len(wordtoSearch)):
                    if wordtoSearch[j] != content[i+j]:
                        break
                else:
                    count += 1
                    returncontent += wordtoReplace
                    i+=len(wordtoSearch)
                    #print(returncontent)
                    continue
                returncontent+=content[i]
                i+=1
            else:
                returncontent+=content[i]
                i+=1
                
    with open('kite_edited.txt','w') as f:
        f.write(returncontent)
    print(content)
    print(returncontent)
    return returncontent,count

print(find_and_replace('hi','bye'))