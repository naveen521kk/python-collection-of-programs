'''Write a function to open a file called El_Shaddai.txt,
 find and print the five most
occurrences of the word from the file'''
def five_most_occurence():
    with open('El_Shaddai.txt') as f:
        content = f.read()
        words = content.split()
    wordsDict = {}
    for i in words:
        i = i.lower()
        if i not in wordsDict:
            wordsDict[i] = 1
        else:
            wordsDict[i]+=1
    words1 = {}
    sync = wordsDict