#read and display I in place of E

tempStr = ''
with open('data1.txt' , 'r') as f:
    content = f.read()
    print("Original File Content:",content)
    for i in content:
        if i=="e":
            tempStr+="i"
        elif i=="E":
            tempStr+="I"
        else:
            tempStr+=i
with open('data1.txt' , 'w') as f:
    f.write(tempStr)
print("Modified Content:",tempStr)