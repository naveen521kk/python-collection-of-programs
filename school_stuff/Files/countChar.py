'''Read the content of a file and count how many time letter `a`
comes in the file'''
search = 'a'
filePath= 'data.txt'
count=0
with open(filePath,'r') as f:
    for i in f:
        for j in i:
            if j.lower() == search:
                count+=1
print(count)

#Alternative counting
with open(filePath,'r') as f:
    print(len([j for i in f for j in i if j.lower() == search]))