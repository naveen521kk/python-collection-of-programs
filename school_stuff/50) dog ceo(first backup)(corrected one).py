'''dog.ceo funny show of dogs '''
#does nothing but just show an image from a url
#first created logic
'''program begins'''

print('Please Wait...')

#import necessary libraries
import os
import time
import json
from io import BytesIO
import random
import webbrowser
from PIL import Image
import sys

try:#checking whether requests lib is present 
    import requests
except:
    print('Do you want to install required python libraries to run this program')
    py_chk=int(input('enter 0/1\n'))
    if py_chk==0:
        print('Bye Bye!!')
        sys.exit()
    else:
        import subprocess
        subprocess.call([sys.executable, "-m", "pip", "install","requests"])
        import requests
        print('Program continuing')

url="https://dog.ceo/api/breeds/image/random"#api location dog-ceo
path=os.path.abspath(os.curdir)+'\\dog_api_images\\'    #getting folder in which program is kept
no_random=random.randint(1,10)     #a random number to save dog
filename="dog"+str(no_random)

while True:
    result=requests.get(url)    #api calls
    a=json.loads(result.text)   #saving result 
    img_url = a["message"]  #getting image url from json result
    img1=requests.get(img_url)  #getting the image from url saved
    img= Image.open(BytesIO(img1.content))  #saving the image code
    if not os.path.exists(path):#creating folder
        os.makedirs(path)
    while os.path.exists(path+filename+'.'+img.format):#checking whether file is present or not
        filename=filename+str(no_random+1)
    open(path+filename+'.'+img.format,"wb").write(img1.content) #saving the file 
    print("Loading...Please Wait!!")
    time.sleep(2)
    img.show()  #showing the saved image
    check=int(input("Do you want to continue.(0 or 1)"))#asking to ccontinue again
    if check==0:
        break
    no_random=random.randint(1,100)
    filename="dog"+str(no_random)
time.sleep(2)
print("Credits Naveen")
print("In partnership with dog.ceo(https://dog.ceo)")
time.sleep(5)
open_dog=int(input('Do you want to open dog.ceo?0/1\n'))
if open_dog==0:
    print('Bye Bye!')
else:
    webbrowser.open('https://dog.ceo')
    sys.exit()


