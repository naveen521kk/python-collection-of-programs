import math
a=int(input('Enter coefficient of x*x'))
b=int(input('Enter coefficient of x'))
c=int(input('Enter coefficient of constant'))
d=math.sqrt((b**2)-(4*a*c))
if d<0:
    print('There is o root')
elif d>=0:
    r1=(-b+d)/2*a
    r2=(-b-d)/2*a
    print('The roots of equation are',r1,r2,sep=' ,')
