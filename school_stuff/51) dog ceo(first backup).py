'''dog.ceo funny show of dogs '''
#does nothing but just show an image from a url
#first created logic
import time
import requests
import json
from PIL import Image
from io import BytesIO
import os
import random
import webbrowser

url="https://dog.ceo/api/breeds/image/random"
path=os.path.abspath(os.curdir)+'\\dog_api_images\\'
no_random=random.randint(1,100)
filename="dog"+str(no_random)

while True:
    result=requests.get(url)
    a=json.loads(result.text)
    img_url = a["message"]
    print(img_url)
    time.sleep(5)
    img1=requests.get(img_url)
    img= Image.open(BytesIO(img1.content))
    if not os.path.exists(path):
        os.makedirs(path)
    while os.path.exists(path+filename+'.'+img.format):
        filename=filename+str(no_random+1)
    open(path+filename+'.'+img.format,"wb").write(img1.content)
    print("Loading...Please Wait!!")
    time.sleep(5)
    img.show()
    check=int(input("Do you want to continue.(0 or 1)"))
    if check==0:
        break
    no_random=random.randint(1,100)
    filename="dog"+str(no_random)
time.sleep(5)
print("Credits Naveen")
print("In partnership with dog.ceo(https://dog.ceo)")
time.sleep(5)
webbrowser.open('https://dog.ceo')


