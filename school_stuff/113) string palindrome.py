#whether a string is a palindrome or not
a=input('Enter a string')
l=len(a)
c=False
if a[0]==a[-1]:
    for i in range(1,l):
        if a[i]==a[-i-1]:
            c=True
        else:
            break
if c:
    print('The string ',a,' is a palindrome')
else:
    print('The string ',a,' is not a palindrome')
