'''Number conversion menu driven program'''
ck=1
while ck:
    print('What do you want to do?')
    print('1.Binary to decimal')
    print('2.Decimal to Binary')
    print('3.Octal to decimal')
    ck=int(input('Enter 1,2,3'))
    if ck==1:
        print('Binary To decimal')
        bin1=input('Enter binary number')
        print('The decimal number for ',bin1,'=',int('0b'+bin1,2))
    elif ck==2:
        print('Decimal to binary')
        dec=int(input('Enter a decimal number'))
        print('The binary number for ',dec,'=',bin(dec))
    elif ck==3:
        print('Octal to decimal')
        oct1=input('Enter a octal number')
        print('The decimal number for ',oct1,'=',int('0o'+oct1,8))
    else:
        print('Please enter valid response')
    print('Do you want to continue?1/0')
    ck=int(input())
