def fn(x=[]):
    x.append(1)
    return x
print(fn())
print(fn())
'''Vidya Mam,
<script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script>
I have a doubt in this question. It was in assignment 6 q 12 g. I at first thought the output is
<pre class="prettyprint lang-py"><code>class Voila {
def fn(x=[]):
    x.append(1)
    return x
print(fn())
print(fn())
</code>
</pre>
[1]
[1]
But on executing the program I got the answer as,
[1]
[1,1]
Mam could you please explain it mam.
'''
