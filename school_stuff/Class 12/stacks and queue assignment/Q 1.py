def check_two_stacks_equal(stack1,stack2):
    if len(stack1)!=len(stack2):
        return False
    else:
        if len(stack2)==0:
            return True
        elif stack1[-1]!=stack2[-1]:
            return False
        else:
            stack1.pop()
            stack2.pop()
            return check_two_stacks_equal(stack1,stack2)
