#implementing queue using stack
stack1 = iterable =[] #This will be the main stack
stack2 = [] #This will be the auxiliary stack.
def enqueue(x):
    while len(stack1)>0:
        stack2.append(stack1.pop())
        stack1.append(x)
    while len(stack2)>0:
        stack1.append(stack2.pop())
    return stack1
def dequeue():
    if len(stack1)==0:
        print("Queue is empty")
    else:
        top=stack1[0]
        stack1.pop()
        return top
