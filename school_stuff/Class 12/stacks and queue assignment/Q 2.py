def queue_reverse(queue,rev=[]):
    if len(queue)!=0:
        first=queue[0]
        queue=queue[1:]
        rev.insert(0,first)
        rev=queue_reverse(queue,rev)
    return rev
