def countListDict(d):
    'This is to count the numbers of list in a Dictionary'
    count=0
    for i in d:
        if type(d[i]) is list:
            count+=1
    return count
print(countListDict({1:[16,5,4,3,5,2],3:{4:5},'l':[2,3,4,5]}))
