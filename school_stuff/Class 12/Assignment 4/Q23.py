def find3HighVal(d):
    l=list(d.values())
    Number=len(l)
    for i in range (Number):
        for j in range(i + 1, Number):
            if(l[i] > l[j]):
                temp = l[i]
                l[i] = l[j]
                l[j] = temp
    return l[0:3]
print(find3HighVal({2:3,6:4,5:6,10:20,3:5}))
