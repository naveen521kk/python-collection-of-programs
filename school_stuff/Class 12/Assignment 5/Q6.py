def primeRange(A,B):
    tup=()
    for n in range(A,B+1):
        isprime=""
        f=n
        if n==2:
            isprime="Is prime."
        else:
            for j in range(2,n):
                if n%j==0:
                    isprime=""
                    break
                else:
                    isprime="Is prime."
        if isprime:
            tup+=(f,)
    else:
        if tup==():
            return "No prime Numbers within given Range"
        else:
            return tup
print(primeRange(6,30))
