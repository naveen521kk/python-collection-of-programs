def luckyNumber(num):
    def chkprime(n):
        isprime=""
        if n==2:
            isprime=True
        else:
            for j in range(2,n):
                if n%j==0:
                    isprime=""
                    break
                else:
                    isprime=True
        if isprime:
            return 1
        else:
            return 0
    n=int(num)
    sumDigits=0
    sumSqDigits=0
    while n>0:
        c=n%10
        sumDigits+=c
        sumSqDigits+=c**2
        n//=10
        print(c)
    if chkprime(sumDigits) and chkprime(sumSqDigits):
        return True
    else:
        return False
print(luckyNumber(16))
        
    
