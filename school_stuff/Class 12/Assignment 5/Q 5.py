'''A#1
C*A*3
E#C#A#5
G*E*C*A*7'''
#patter printing in functions
def printRandom(n):
    num=1
    for i in range(n):
        if i%2:
            for j in range(num+64,64,-1):
                if j%2:
                    print(chr(j),end='')
                    print('*',end='')
            print(num)
        else:
            for j in range(num+64,64,-1):
                if j%2:
                    print(chr(j),end='')
                    print('#',end='')
            print(num)
        num+=2
printRandom(8)
