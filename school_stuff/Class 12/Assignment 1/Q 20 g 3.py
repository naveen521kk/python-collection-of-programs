def s(x,n):
    def fact(m):
        if m==0:
            return 1
        else:
            return m * fact(m-1)
    if n==0:
        return 0
    else:
        return x**(2*n-1)/fact(2*n+1) + s(x,n-1)
print(s(1,3))
