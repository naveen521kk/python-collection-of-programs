#sum of series
#$\frac{x}{1}+\frac{x^2}{2!}+\frac{x^3}{3!}+...+\frac{x^n}{n!}$
def factorial(n):
    if n==1:
        return 1
    else:
        return n*factorial(n-1)
def factorialNonRecurssionn(n):
    fact=1
    for i in range(1,n+1):
        fact*=i
    return fact
def sumSeries(x,n):
    sum1=0
    for i in range(1,n+1):
        fact=factorial(i)
        sum1+=(x**i)/fact
    return sum1
def sumSeriesRecurssion(x,n):
    if n<1:
        return 0
    else:
        return (x**(n))/factorial(n)+sumSeriesRecurssion(x,n-1)
print(sumSeriesRecurssion(4,6))
print(sumSeries(4,6))
