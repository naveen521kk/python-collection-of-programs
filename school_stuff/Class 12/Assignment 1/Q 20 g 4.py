#$\frac{x}{2!}+\frac{x^3}{4!}+\frac{x^5}{6!}+...+\frac{x^{2n-1}}{2n!}$
def factorial(n):
    if n==0:
        return 1
    else:
        return n*factorial(n-1)
def sumSeries(x,n):
    if n==1:
        return x/2
    else:
        if n%2:
            return (x**(2*n-1)/factorial(2*n)) + sumSeries(x,n-1)
        else:
            return -1*(x**(2*n-1)/factorial(2*n)) + sumSeries(x,n-1)
def sumSeries1(x,n):
    if n == 1: return x/2
    else:
        return (-1**n)*((x**(2*n-1))/factorial(2*n)) + sumSeries(x,n-1)
print(sumSeries(2,4))
print(sumSeries1(2,4))
