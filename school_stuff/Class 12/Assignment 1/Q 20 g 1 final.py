#sum of series
#$\frac{x}{1}+\frac{x^2}{2!}+\frac{x^3}{3!}+...+\frac{x^n}{n!}$
def factorial(n):
    if n==1:
        return 1
    else:
        return n*factorial(n-1)
def sumSeriesRecurssion(x,n):
    if n<1:
        return 0
    else:
        return (x**(n))/factorial(n)+sumSeriesRecurssion(x,n-1)
print(sumSeriesRecurssion(4,6))
