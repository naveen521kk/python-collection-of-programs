#sum of series
#$\frac{x}{1}-\frac{x^2}{2!}+\frac{x^3}{3!}-...\pm\frac{x^n}{n!}$
def factorial(n):
    if n==1:
        return 1
    else:
        return n*factorial(n-1)
def sumSeries(x,n):
    sum1=0
    for i in range(1,n+1):
        fact=factorial(i)
        if i%2:
            sum1+=(x**i)/fact
        else:
            sum1-=(x**i)/fact
    return sum1
def sumSeriesRecurssion(x,n):
    if n<1:
        return 0
    else:
        if n%2:
            return (x**(n))/factorial(n)+sumSeriesRecurssion(x,n-1)
        else:
            return -1*(x**(n))/factorial(n)+sumSeriesRecurssion(x,n-1)
print(sumSeries(5,6))
print(sumSeriesRecurssion(5,6))
