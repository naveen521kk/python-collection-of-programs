from cryptography.fernet import Fernet
key = Fernet.generate_key()
file = open('key.key', 'wb')
file.write(key) # The key is type bytes still
file.close()
'''Reading keys
file = open('key.key', 'rb')
key = file.read() # The key will be type bytes
file.close()'''

'''To create a key using password
import base64
import os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

password_provided = "password" # This is input in the form of a string
password = password_provided.encode() # Convert to type bytes
salt = b'salt_' # CHANGE THIS - recommend using a key from os.urandom(16), must be of type bytes
kdf = PBKDF2HMAC(
    algorithm=hashes.SHA256(),
    length=32,
    salt=salt,
    iterations=100000,
    backend=default_backend()
)
key = base64.urlsafe_b64encode(kdf.derive(password)) # Can only use kdf once'''

#Encryption begins
message = """Hello World\t\a\t\a\a\a""".encode()

f = Fernet(key)
encrypted = f.encrypt(message)
with open("easy_encrypt.naveen", 'wb') as f:
    f.write(encrypted)
with open("easy_encrypt.naveen","rb") as f:
    data = f.read()

fernet = Fernet(key)
encrypted = fernet.decrypt(data)
print(encrypted.decode())

