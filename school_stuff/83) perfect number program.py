#To check a perfect number or not
'''What is perfect number?
In number theory, a perfect number is a positive integer that is equal to the sum of its positive divisors,
excluding the number itself. For instance, 6 has divisors 1, 2 and 3, and 1 + 2 + 3 = 6,
so 6 is a perfect number.'''

n=int(input('Enter a number'))
sum_factors=0
for i in range(1,n):
    if n%i==0:
        sum_factors+=i
if sum_factors==n:
    print('It is a perfect number')
else:
    print('It is not a perfect number')

