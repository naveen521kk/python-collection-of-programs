def decToBinNonRecursion(dec):
    temp=dec
    c=0
    res=0
    while temp:
        d=temp%2
        res+=d*10**c
        temp//=2
        c+=1
    return res
def binary_to_decimal(bstring):
    if not bstring:
        return 0
    return binary_to_decimal(bstring[:-1]) * 2 + int(bstring[-1])
def decToBin( decimal_number ): 
    if decimal_number == 0: 
        return 0
    else:
        print(decimal_number,decimal_number//2)
        return (decimal_number % 2 + 10 * decToBin(decimal_number // 2))
import timing
print(decToBin(35))
#print(timing.endlog())
#print(decToBinNonRecursion(35))
print(timing.endlog())
#print(binary_to_decimal('100'))
