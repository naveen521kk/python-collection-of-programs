#$x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}........ n terms$
def factorial(n):
    if n==1:
        return 1
    else:
        return n*factorial(n-1)
def sumSeries(x,n):
    if n == 1:
        return x
    else:
        #return (-1**n)*((x**(2*n-1))/factorial(2*n-1)) + sumSeries(x,n-1)
        #return (-1**(n-2))*((x**(2*n-1))/factorial(2*n-1)) + sumSeries(x,n-1)
        if n%2:
            return (x**(2*n-1)/factorial(2*n-1)) + sumSeries(x,n-1)
        else:
            return -1*(x**(2*n-1)/factorial(2*n-1)) + sumSeries(x,n-1)
#correct
def sumSeries1(x,n):
    if n == 1:
        return x
    else:
        if n%2:
            return (x**(2*n-1)/factorial(2*n-1)) + sumSeries(x,n-1)
        else:
            return -1*(x**(2*n-1)/factorial(2*n-1)) + sumSeries(x,n-1)
print(sumSeries(1,5))

