def binarySearch(lst,item,low,high):
    if low>high:
        return -1
    mid=int((low+high)/2)
    if lst[mid]==item:
        return mid+1 #starts from 0
    elif lst[mid]>item:
        high=mid-1
        return binarySearch(lst,item,low,high)
    else:
        low=mid+1
        return binarySearch(lst,item,low,high)
lst=eval(input("Enter a list"))
lst.sort()
print(lst)
item=int(input("Enter the item to search."))
print(binarySearch(lst,item,0,len(lst)-1))
