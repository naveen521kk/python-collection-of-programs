'''WAP to get the difference between the two lists.'''
l1=eval(input('Enter first list'))
l2=eval(input('Enter second list'))
if len(l1)>len(l2):
    l3=l2
    l2=l1
    l1=l3
    #equivalent to
    '''l1,l2=l2,l1'''
for i in l1:
    if i in l2:
        l2.remove(i)
print('Difference=',l2)
