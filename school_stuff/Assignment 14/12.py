'''WAP to print a specified list after removing the 0th, 4th and 5th elements.
Sample List : ['Red', 'Green', 'White', 'Black', 'Pink', 'Yellow']
Expected Output : ['Green', 'White', 'Black']'''
l=eval(input('Enter list'))
l.pop(0)
l.pop(3)
l.pop(3)
print(l)
