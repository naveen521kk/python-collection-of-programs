'''WAP to display all positive elements from left to right and negative element from right to left and 0 in the
middle.
Sample List : [2, -5, 1,0,-3,-2,5 ] Expected Result : [2,1,5,0,-2,-3,-5]'''
l=eval(input('Enter a list of number which you need to arrange in left to right order'))
pl=[];nl=[];zl=[]
for i in l:
    if i>0:
        pl.append(i)
    elif i==0:
        zl.append(i)
    else:
        nl.append(i)
nl=nl[::-1]
print(pl+zl+nl)
        
