"""Python API wrapper for sxcu.net subdomain
"""
import requests
import json

# token=d6208525-a58a-4b91-8dc3-bcb7ad086445
# dom=pls.click-if-you-da.re


class og_properties(object):
    def __init__(self, color=None, description=None, title=None):
        self.color = color
        self.description = description
        self.title = title

    def export(self):
        return json.dumps(
            {
                "color": self.color,
                "title": self.title,
                "description": self.description,
            }
        )


class SCXU:
    """The Main class for scxu.net request"""

    def __init__(
        self, subdomain: str = None, upload_token: str = None, file_scxu: str = None
    ):
        """This initialise the class

        Parameters
        ==========
        subdomain : :class:`str`, optional
            The subdomain you get from scxu.net
        upload_token : :class:`str`, optional
            The upload token that comes along with subdomain
        file_scxu : :class:`str`,optional
            The scxu file you have got.
        """
        self.subdomain = subdomain if subdomain else "https://sxcu.net"
        self.upload_token = upload_token
        self.file_scxu = file_scxu
        if file_scxu:
            with open(file_scxu) as f:
                con = json.load(f)
            self.subdomain = con["RequestURL"]

    def upload_image(
        self,
        file: str,
        collection: str = None,
        collection_token: str = None,
        noembed: bool = False,
        og_properties: str = None,
    ):
        """This uploads image to SCXU

        Parameters
        ==========
        file : :class:`str`, optional
            The path of File to Upload
        collection : :class:`str`, optional
            The collection ID to which you want to upload to if you want to upload to a collection
        collection_token : :class:`str`, optional
            The collection upload token if one is required by the collection you're uploading to.
        noembed : :class:`bool`, optional
            If ``True``, the uploader will return a direct URL to the uploaded image, instead of a dedicated page.
        og_properties : :class:`og_properties`, optional
            This will configure the OpenGraph properties of the file's page, effectively changing the way it embeds in various websites and apps.
        
        Returns
        =======
        :class:`dict`
            The returned JSON from the request.
        """
        files = {"image": open(file, "rb")}
        data = {}
        if self.upload_token:
            data["token"] = self.upload_token
        if collection:
            data["collection"] = collection
        if collection_token:
            data["collection_token"] = collection_token
        if noembed:
            data["noembed"] = ""
        if og_properties:
            data["og_properties"] = og_properties.export()
        url = (
            self.subdomain + "upload"
            if self.subdomain[-1] == "/"
            else self.subdomain + "/upload"
        )
        res = requests.post(url=url, files=files, data=data)
        return res.json()


if __name__ == "__main__":
    a = SCXU()
    b = og_properties("#7289DA", "Some title", "A cool description!")
    c = a.upload_image("sample.png", og_properties=b, noembed=True)
    print(c)