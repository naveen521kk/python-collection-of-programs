'''Python Program to check whether the given string is a palindrome or not without using built in function'''
str1=input("Enter a string")
rev=''
for i in range(len(str1)-1,-1,-1):
    rev+=str1[i]
if str1==rev:
    print("%s is a palindrome"%str1)
else:
    print("%s is not a palindrome"%rev)
