'''Python Program to Find Armstrong Number in an Interval(limit)
      Input : Enter the starting and ending limit 100-   1000'''
lo=int(input("Enter starting value"))
up=int(input("Enter end value"))
armno=[]
for i in range(lo,up+1):
    temp=i
    s=0
    while temp>0:
        r=temp%10
        temp=temp//10
        s+=r**3
    if s==i:
        armno.append(i)
if len(armno)==0:
    print("No numbers found")
else:
    print("Armstrong numbers: ",end='')
    for i in armno:
        print(i,end='  ')
    
