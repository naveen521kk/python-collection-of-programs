'''Get first, second best scores from the list without using sort(). List may contain duplicates.             Ex: [86,86,85,85,85,83,23,45,84,1,2,0] => should get 86, 85'''
l=eval(input('Enter a list?'))
n=len(l)
for i in range(n):
    for j in range(0,n-i-1):
        if l[j]<l[j+1]:
            l[j],l[j+1]=l[j+1],l[j]
print(l)
print("First:",l[0])
j=0
while l[j]==l[j+1]:
    j+=1
print(j)
print("Second:",l[j+1])
