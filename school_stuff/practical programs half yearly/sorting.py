'''You are required to write a program to sort the (name, age, height) tuples by ascending order where name is string, age and height are numbers. The tuples are input by console. The sort criteria is:
1: Sort based on name;
2: Then sort based on age;
3: Then sort by score.
The priority is that name > age > score.
'''
tup=eval(input('Enter tuples'))#(('Tharun',19,80),('John',20,90),('Jony',17,91),('Jony',17,93),('Json',21,85))

lst=[]
for i in tup:
    lst.append(i)
print(lst)
#sort based on score
n=len(lst)
for i in range(n):
    for j in range(0,n-i-1):
        if lst[j][2]>lst[j+1][2]:
            lst[j],lst[j+1]=lst[j+1],lst[j]
print(lst)
#sort based on age
n=len(lst)
for i in range(n):
    for j in range(0,n-i-1):
        if lst[j][1]>lst[j+1][1]:
            lst[j],lst[j+1]=lst[j+1],lst[j]
print(lst)
#sort based on names
n=len(lst)
for i in range(n):
    for j in range(0,n-i-1):
        if lst[j][0]>lst[j+1][0]:
            lst[j],lst[j+1]=lst[j+1],lst[j]


print(lst)
