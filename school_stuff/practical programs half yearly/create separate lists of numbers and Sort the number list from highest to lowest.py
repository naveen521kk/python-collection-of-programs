'''         gadgets = [“Mobile”, “Laptop”, 100, “Camera”, 310.28, “Speakers”, 27.00,
                          “Television”, 1000, “Laptop Case”, “Camera Lens”]       
create separate lists of numbers and Sort the number list from highest to lowest
              using bubble sort
'''
gadgets = ['Mobile', 'Laptop', 100, 'Camera', 310.28, 'Speakers', 27.00,'Television', 1000, 'Laptop Case', 'Camera Lens']
strgad=[]
numgad=[]
#getting string and numbers in seperate list
for i in gadgets:
    if type(i) is str:
        strgad.append(i)
    else:
        numgad.append(i)
#bubble sort
n=len(numgad)
for i in range(n):
    for j in range(0,n-i-1):
        if numgad[j]<numgad[j+1]:
            numgad[j],numgad[j+1]=numgad[j+1],numgad[j]
print(numgad)
