'''A website requires the users to input username and password to register. Write a program to check the validity of password input by users.
Following are the criteria for checking the password:
1. At least 1 letter between [a-z]
2. At least 1 number between [0-9]
1. At least 1 letter between [A-Z]
3. At least 1 character from [$#@]
4. Minimum length of transaction password: 6
5. Maximum length of transaction password: 12
Your program should accept a password and  check  whether it satisfies  the above criteria. Example
'''
usrname=input('Enter your username')
password=input('Enter your account password')
pass_chk={'smallchr':False,
          'num':False,
          'capchr':False,
          'specchr':False,
          'minlen':False,
          'maxlen':True}
smlist=[];caplist=[];numlist=[]
spechr=['$','#','@']
valid=False
for i in range(97,123):
    smlist.append(chr(i))
for i in range(65,91):
    caplist.append(chr(i))
for i in range(0,10):
    numlist.append(str(i))
#main checking begins
for i in password:
    if i in smlist:
        pass_chk['smallchr']=True
    elif i in caplist:
        pass_chk['capchr']=True
    elif i in numlist:
        pass_chk['num']=True
    elif i in spechr:
        pass_chk['specchr']=True
    if pass_chk['num']==True and pass_chk['smallchr']==True and pass_chk['capchr']==True and pass_chk['specchr']==True:
        break
if len(password)>=6:
    pass_chk['minlen']=True
if len(password)>=12:
    pass_chk['maxlen']=False
for i in pass_chk:
    if pass_chk[i]==True:
        valid=True
    else:
        valid=False
        print('Not Valid')
        break
else:
    print("Valid")
print(pass_chk)

