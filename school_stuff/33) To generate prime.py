'''To generate prime or composite numbers within a given range'''
l=int(input('Enter a lower limit'))
u=int(input('Enter upper limit'))
for n in range(l,u+1):
    isprime=""
    print(n,end='\t')
    if n==2:
        isprime="Is prime."
    else:
        for j in range(2,n):
            if n%j==0:
                isprime=""
                break
        else:
                isprime="Is prime."
    if isprime:
        print(isprime)
    else:
        print('Not a prime')


