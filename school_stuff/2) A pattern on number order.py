'''when n=5
    1 
   1 2 
  1 2 3 
 1 2 3 4 
1 2 3 4 5
'''
#shows a triangle of numbers in series

n=int(input())#number of lines and till what
for i in range(1,n+1):#loop for number of lines
    for k in range(n+1-i,1,-1): #A loop for printing space
        print(' ',end='')
    for j in range(1,i+1):#print number after space
        print(j,end=' ')
    print()

    


