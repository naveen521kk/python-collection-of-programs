'''Create the following list using a for loop'''
#pg 341 5

#a) A list consisting of integers 0 through 49
lst_int=[]
for i in range(50):
    lst_int.append(i)
print(lst_int)

#b)A list containing the squares of the integers 1 through 50
import math
lst_squ=[]
for i in range(1,50+1):#wrote 50+1 explicitely for understanding
    lst_squ.append(int(math.pow(i,2)))#i**2
print(lst_squ)

#c) The list ['a', 'bb', 'ccc', 'dddd', 'eeeee', 'ffffff', 'ggggggg',. . .] that ends with 26 copies of the letter z.
lst_alpha=[]
a=ord('a');z=ord('z')
for i in range(1,26+1):
    cha=a+i-1
    lst_alpha.append(chr(cha)*i)
print(lst_alpha)
    
