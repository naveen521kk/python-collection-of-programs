from flask import Flask, flash, redirect, render_template, request, session, abort

app = Flask(__name__)

@app.route("/")
def index():
    return "Flask App!"

@app.route("/hello/<string:name>/")
def hello(name):
    #url_for('static', filename='style.css')
    return render_template(
    'test.html',name=name)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
