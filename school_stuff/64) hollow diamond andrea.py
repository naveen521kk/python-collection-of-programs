def hollowdiamond(columns):
    if columns%2==0:
        print("An even number of columns will result in a flat-topped diamond. Rounding up to odd...")
        print((columns+1)*"*")
    else:
        print(columns*"*")
    sp=1 #Inital number of spaces that form the triangle
    sa=(columns//2) #initial number of * on either side of space.
    for i in range(0,sa-1): #The loop stops exactly before the single asterisks on either side are printed. (sa-1 means loop goes until 2nd to 
                            #last cycle, and will not print the single asterisks on either side)
        if sa!=1:
            print(sa*"*"+sp*" "+sa*"*")
        else:
            print(sa*"*"+sp*" "+sa*"*",end="")
        sa-=1 #subtract 1 from asterisks, for a progression.
        sp+=2 #Add 2 to spaces, one for each asterisk used up
    for i in range(sp//2,sp,1):
         print(sa*"*"+sp*" "+sa*"*")
         sa+=1 #subtract 1 from asterisks, for a progression.
         sp-=2 #Add 2 to spaces, one for each asterisk used up
        #  if sp<0:
        #      print("Something's wrong.")
    if columns%2==0:
        print((columns+1)*"*")
    else:
        print(columns*"*")

hollowdiamond(int(input("Enter number of columns:")))
