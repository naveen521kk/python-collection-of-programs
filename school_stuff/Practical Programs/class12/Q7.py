"""Write a program to perform Queue operations on Nested list 
containing country name and capital 
a.insertion  
b.deletion. """


def Enqueue(Q, item):
    """This function add to ``item`` to a Queue ``Q``"""
    global front, rear
    Q.append(item)
    if len(Q) == 1:
        front = rear = 0
    else:
        rear = len(Q) - 1


def Dequeue(Q):
    """This function remove the item from Queue"""
    global front, rear
    if len(Q) == 0:
        print("Underflow")
        return
    else:
        val = Q.pop(0)
    if len(Q) == 0:
        front = rear = 0
    print("\nDeleted item was", val)


def Peek(S):
    global front
    """This function shows the First of the Queue.
    """
    if len(Q) == 0:
        print("Underflow")
    else:
        front = 0
        return Q[front]


def Show(S):
    """This function prints the Queue"""
    if len(Q) == 0:
        print("Underflow")
    else:
        print("(Front)", end=" ")
        front = 0
        i = front
        rear = len(Q) - 1
        while i <= rear:  #   for t in range(len(S)-1,-1,-1):
            print(Q[i], "<--", end=" ")  #  print(S[t],"<--", end=' ')
            i += 1
        print()


Q = []  # intialisation
front = rear = None
while True:
    print(
        "What do you want to do?",
        "1. Insert a Country and Capital Name to the Queue",
        "2. Delete a country name and capitable in a Queue.",
        "3. Show the countries in Queue",
        "0. Exit",
        sep="\n",
        end=":",
    )
    ch = int(input())
    if ch == 1:
        while True:
            country = input("Enter Country Name:")
            capital = input("Enter Capital Name:")
            final_list = [country, capital]
            Enqueue(Q, final_list)
            cur = input("Do you want to add more item?[Y/n]")
            if cur.lower() == "n":
                break
    elif ch==2:
        Dequeue(Q)
    elif ch==3:
        Show(Q)
    elif ch==0:
        break
    
