"""Store subscriber name & phone in dictionary. 
Perform as menu driven code & UDFs add, view, modify,
delete a subscriber
"""


def addSub(database, phone, name):
    database[phone] = name
    return database


def delelteSub(database, phone):
    if phone in database:
        del database[phone]
    else:
        print(f"{phone} subscriber doesn't exist.")
    return database


def modifySub(
    database,
    ophone,
    name=None,
    nphone=None,
):
    if nphone is None:
        database[ophone] = name
        return database
    elif name is None:
        name = database[ophone]
        delelteSub(database, ophone)
        database[nphone] = name
        return database


def showSubs(database):
    for data in database:
        print(data, database[data], sep="\t")


while True:
    print(
        "1. Add a Subscriber",
        "2. View Subscribers",
        "3. Modify a Subscriber",
        "4. Delete a Subscriber",
        sep="\n",
        end="",
    )