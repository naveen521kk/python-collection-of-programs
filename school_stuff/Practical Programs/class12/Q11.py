"""Create a binary file EMPLOYEE.Dat with n records
( having fields name,age,dept.,designation,salary)  
and perform the following options
1. Display details of Managers earning more than 50000 in Finance
and in Admin Dept.
2. Delete the employee details who have reached (58) retirement age.
"""

import pickle

"""
records = [
    {
        "name": "Ram",
        "age": "29",
        "dept": "Marketing",
        "designation": "Manager",
        "salary": "50000",
    },
    {
        "name": "Sonu",
        "age": "45",
        "dept": "HR",
        "designation": "Manager",
        "salary": "40000",
    },
]"""  # This is how records look

records = []  # this is the main list

while True:
    print(
        "What do you want to do?",
        "1.Input details of employee's",
        "2.Display details of Managers earning more than 50000 in Finance and in Admin Dept.",
        "3.Delete the employee details who have reached (58) retirement age.",
        "0.Exit",
        sep="\n",
        end=":",
    )
    ch = int(input())
    if ch == 1:
        print("Input the employee's details")
        while True:
            dict_temp = {}
            for req in ["name", "age", "dept", "designation", "salary"]:
                dict_temp[req] = input("Enter employee's %s:" % req)
            records.append(dict_temp)
            con = input("Do you want to add more record?:[Y/n]")
            if con.lower().strip() == "n":
                break
        print("No. of records written:", len(records))
        with open("EMPLOYEE.Dat", "wb") as f:
            print("Dumping into the file..")
            pickle.dump(records, f)
    elif ch == 2:
        print("Loading Data")
        with open("EMPLOYEE.Dat", "rb") as f:
            records = pickle.load(f)
        for emp in records:
            if (int(emp["salary"]) > 50000) and (emp["dept"].lower() in ["finance", "admin"]):
                print("-------------------------")
                for key,det in zip(emp.keys(),emp.values()):
                    print("{} - {}".format(key,det))
                print("-------------------------")
    elif ch == 3:
        print("Loading Data")
        with open("EMPLOYEE.Dat", "rb") as f:
            records = pickle.load(f)
        for emp in range(len(records)):
            if int(records[emp]["age"])>58:
                print("deleting {}".format(records[emp]["name"]))
                records.pop(emp)
        with open("EMPLOYEE.Dat", "wb") as f:
            print("Dumping into the file..")
            pickle.dump(records, f) 
    elif ch == 0:
        break
print("Program Completed")
