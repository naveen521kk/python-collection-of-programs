"""Create a binary file STUDENT.DAT/.BIN/.TXT with roll,name stream and
total marks using menu:
1 Display the topper detail wrt each stream
2.increment the total by 3 marks for student having biology stream and decrement by 2 in EG stream.
"""


import pickle

"""
records = [
    {
        "roll": "2",
        "name": "Ram",
        "stream": "cs", #cs,biology,eg
        "t_marks": "468",
    },
    {
        "roll": "3",
        "name": "Sonu",
        "stream": "biology",
        "t_marks": "468",
    },
]"""  # This is how records look

records = []  # this is the main list

while True:
    print(
        "What do you want to do?",
        "1.Input details of Students's",
        "2.Display the topper detail wrt each stream.",
        "3.increment the total by 3 marks for student having biology stream and decrement by 2 in EG stream.",
        "0.Exit",
        sep="\n",
        end=":",
    )
    ch = int(input())
    if ch == 1:
        print("Input the Students's details")
        while True:
            dict_temp = {}
            for req in ["roll", "name", "stream", "t_marks"]:
                if req in ["roll", "t_marks"]:
                    dict_temp[req] = int(input("Enter Students's %s:" % req))
                else:
                    dict_temp[req] = input("Enter Students's %s:" % req)
            records.append(dict_temp)
            con = input("Do you want to add more record?:[Y/n]")
            if con.lower().strip() == "n":
                break
        print("No. of records written:", len(records))
        with open("STUDENT.Dat", "wb") as f:
            print("Dumping into the file..")
            pickle.dump(records, f)
    elif ch == 2:
        print("Loading Data")
        with open("STUDENT.Dat", "rb") as f:
            records = pickle.load(f)
        streams = []
        for stu in records:
            if stu["stream"] not in streams:
                streams.append(stu["stream"])
        max_total = 0
        max_student_detail = {}
        for stre in streams:
            max_total = 0
            max_student_detail = {}
            for stu in records:
                if stu["stream"] == stre:
                    if stu["t_marks"] > max_total:
                        max_total = stu["t_marks"]
                        max_student_detail = stu

            print("The topper in %s is:" % stre)
            for key, det in zip(max_student_detail.keys(), max_student_detail.values()):
                print("{} - {}".format(key, det))
    elif ch == 3:
        print("Loading Data")
        with open("STUDENT.Dat", "rb") as f:
            records = pickle.load(f)
        for stu in range(len(records)):
            print(records[stu])
            if records[stu]["stream"] == "bio":
                records[stu]["t_marks"] += 3
            elif records[stu]["stream"] == "eg":
                records[stu]["t_marks"] -= 2
        print("Updated the Marks")
        with open("STUDENT.Dat", "wb") as f:
            print("Dumping into the file..")
            pickle.dump(records, f)
    elif ch == 0:
        break
print("Program Completed")
