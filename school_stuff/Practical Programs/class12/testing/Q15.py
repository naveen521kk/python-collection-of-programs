import mysql.connector

database = "student"
table_name = "stu"
def check_database(name=database):
    cur.execute("show databases;")
    for i in cur.fetchall():
        if i[0] == name:
            cur.execute(f"DROP database {name};")
            break
    cur.execute(f"CREATE DATABASE {database};")
    cur.execute(f"USE {name};")

con = mysql.connector.connect(host="localhost",user="root",passwd="password21")

cur = con.cursor()
check_database()

create_table = f"""\
CREATE TABLE if not exists {table_name}(
roll INT PRIMARY KEY,
name CHAR(20) NOT NULL,
total INT,
course CHAR(20) NOT NULL,
DOB DATE
);
"""
cur.execute(create_table)

cur.execute(f"truncate {table_name}") #cleans table


template = "INSERT INTO %s VALUES({sno},'{name}',{mark},'{sub}','{date}')"%table_name

cur.execute(template.format(sno=1,name="Arun",mark=198,sub="computer",date="2003-05-19"))
cur.execute(template.format(sno=2,name="Raj",mark=450,sub="eg",date="2003-07-29"))
cur.execute(template.format(sno=3,name="Sam",mark=450,sub="bio",date="2003-02-05"))
cur.execute(template.format(sno=4,name="Ajay",mark=430,sub="computer",date="2002-11-18"))
cur.execute(template.format(sno=5,name="Harsh",mark=420,sub="eg",date="2004-01-25"))

con.commit()


cur.execute(f"SELECT * from {table_name}")
print(cur.fetchall())

cur.execute(f"select max(total),min(total),count(course) from {table_name} group by course having count(course)<2;")
print("1",cur.fetchall())

cur.execute(f"select * from {table_name} where DOB like '2003-05-%' and total>100 and total<200;")
print("2",cur.fetchall())

cur.execute(f"select * from {table_name} order by total desc;")
print("3",cur.fetchall())

cur.execute(f"update {table_name} set total=total+(0.15*total)where course='computer' and total<180;")
cur.execute (f"select * from {table_name} where course='computer';")
print('4',cur.fetchall())
