'''AIM:
Write user defined functions with tuple passed as argument
(a)show words ahving all 5 vowels
(b)nested tuple each element having height,weight using functions taking on
   element at a time to check BMI and return OBESE/NORMAL/UNDERWEIGHT'''



def show(tu):
    v=['a','e','i','o','u']
    vo=['A','E','I','O','U']
    for i in range(len(tu)):
        c=0
        st=list(tu[i])
        new=[]
        for j in range(len(st)):
            if st[j] in new:
                pass
            else:
                new.append(st[j])
                if st[j] in v or st[j] in vo:
                    c+=1
        if c==5:
            print(tu[i])

def BMI(tu):
    for i in range(len(tu)):
        b=tu[i][1]/(tu[i][0]*tu[i][0])
        if b>=25.0:
            print("overweight")
        elif 18.5<b<24.9:
            print("normal")
        else:
            print("underweight")
print('''MENU
1.show words having all 5 vowels
2.to chcek bmi''')
ch=int(input("enter choice:"))
if ch==1:
    li=[]
    l=int(input("enter no of terms:"))
    for i in range(l):
        e=input("enter word:")
        li.append(e)
    t=tuple(li)
    show(t)
elif ch==2:
    l=[]
    n=int(input("enter no of terms:"))
    for i in range(n):
        li=[]
        w=float(input("enter weight in Kg:"))
        h=float(input("enterheight in meter:"))
        li.append(h)
        li.append(w)
        l.append(tuple(li))
    t=tuple(l)
    BMI(t)
else:
    print("wrong choice")

'''OUTPUT:

(a)MENU
1.show words having all 5 vowels
2.to chcek bmi
enter choice:1
enter no of terms:4
enter word:aeio
enter word:aeiou
enter word:ramesh
enter word:Karthik
aeiou

(b)MENU
1.show words having all 5 vowels
2.to chcek bmi
enter choice:2
enter no of terms:3
enter weight in Kg:60
enterheight in meter:1.5
enter weight in Kg:90
enterheight in meter:1.8
enter weight in Kg:110
enterheight in meter:1.9
overweight
overweight
overweight
'''
