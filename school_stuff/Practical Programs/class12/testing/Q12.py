def check_vowels(tup):
    """This function shows the words which has 5 vowels in it."""
    for i in tup:
        for j in "aeiou":
            if j in i.lower():
                pass
            else:
                break
        else:
            print("%s has all 5 vowels" % i)


def calculate_bmi(n_tuple):
    """Calculate BMI for each person in the ``n_tuple`` and return
    about their health assumes weight is in ``kg`` and height is in ``m``"""
    for person in n_tuple:
        height, weight = person
        bmi = weight / (height) ** 2
        if bmi < 18.5:
            print("Under Weight")
        elif bmi >= 18.5 and bmi < 25:
            print("Normal")
        elif bmi > 25:
            print("Overweight")


if __name__ == "__main__":
    words = tuple()
    n = int(input("Number of words?"))
    for _ in range(n):
        vowel = input("Enter word to check Vowel:")
        words += (vowel,)
    check_vowels(words)

    tup = tuple()
    n = int(input("Number of persons:"))
    for i in range(n):
        h = float(input("height in m:"))
        w = float(input("weigth in kg:"))
        tup += ((h, w),)
    calculate_bmi(tup)
