"""WAP to compute the mean ,median and mode of the list of numbers 
using statistical functions, passing lists to functions.
"""
import statistics
while True:
    print("What do you want to do for the data you are entering?\n Calculate:\n","1. Mean","2. Median","3. Mode","0. Exit",sep="\n",end="\n")
    inp = int(input("Response:"))
    if inp == 0:
        print("Program Completed")
        break
    else:
        datano = int(input("Enter no of data:"))
        data = []
        for i in range(1, datano + 1):
            data.append(int(input("Enter {} data:".format(i))))
        if inp == 1:
            # mean calculation
            mean = statistics.mean(data)
            print("The Mean is %s"%mean)
        elif inp==2:
            #Median calculation
            median = statistics.median(data)
            print("The median is %s"%median)
        elif inp==3:
            #mode calculation
            mode = statistics.mode(data)
            print("The mode is %s"%mode)
        else:
            print("Please give a valid input <1,2,3,0>")
            continue
        print("Do you want to continue.[Y/n]",end='')
        con=input()
        if con.lower() == "y":
            continue
        else:
            print("Program Completed")
            break
