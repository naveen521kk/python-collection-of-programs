"""WAP using menu to create text file
a.to count no of words.
b.palindrome check. 
c.word ending and beginning with a vowel.
"""
while True:
    print(
        "Enter Your Choice",
        "1. Check no of words",
        "2. Check palindrome",
        "3. Words beginning and ending with vowels",
        "0. Exit",
        sep="\n",
        end="",
    )
    ch = int(input())
    if ch == 0:
        break
    else:
        path = input("Enter Path of Text File to Read:")
        if ch == 1:
            with open(path, "r") as f:
                content = f.read().split()
                print("No.of.words=", len(content))
        elif ch == 2:
            with open(path, "r") as f:
                content = f.read()
                for i in content.split():
                    if i[::-1] == i:
                        print(i, "is a palindrome")
        elif ch == 3:
            with open(path, "r") as f:
                content = f.read()
                for i in content.split():
                    if i[0].lower() in "aeiou" and i[-1].lower() in "aeiou":
                        print(i, "is a word beginning and ending with a Vowel")
print("Program Completed")
