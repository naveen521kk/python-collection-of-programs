""" Write UDFs with tuple passed as arg  Tuple of words 
a.show words having all 5 vowels 
b. Nested tuple each element having height,weight. Using function
taking one element at a time to check BMI & return OBESE/ NORMAL
/UNDER WEIGHT"""


def check_vowels(tup):
    """This function shows the words which has 5 vowels in it."""
    for i in tup:
        for j in "aeiou":
            if j in i:
                pass
            else:
                break
        else:
            print("%s has all 5 vowels" % i)

def calculate_bmi(n_tuple):
    """Calculate BMI for each person in the ``n_tuple`` and return
    about their health assumes weight is in ``kg`` and height is in ``m``"""
    for person in n_tuple:
        height,weight=person
        bmi=weight/(height)**2
        if bmi<18.5:
            return "Under Weight"
        elif bmi>=18.5 and bmi<25:
            return "Normal"
        elif bmi>25:
            return "Overweight"



