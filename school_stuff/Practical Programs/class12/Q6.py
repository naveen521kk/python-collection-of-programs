"""Write a program to perform operations on stack (using list of integers) 
a.push 
b.pop"""


def is_empty(stk):
    if top == 0:
        return True
    else:
        return False


def push(stk, item):
    global top
    stk.append(item)
    top = len(stk) - 1


def pop(stk):
    global top
    if is_empty(stk):
        return "Underflow"
    else:
        item = stk.pop()
        if len(stk) == 0:
            top = None
        else:
            top = len(stk) - 1
        return item


stack = []
top = None
while True:
    print("1.Push", "2.Pop", "0.Exit", end=":", sep="\n")
    ch = int(input())
    if ch == 1:
        item = int(input("enter the item"))
        push(stack, item)
    elif ch == 2:
        item = pop(stack)
        if item == "Underflow":
            print("underflow")
        else:
            print("popped item=", item)
    elif ch == 0:
        break
    else:
        print("Invalid")

print("Program Over")