"""Generate library functions to :
a) display palindrome nos in tuple.
"""
def palindrome(x,y):
    """This function return the palindrome number in the specified range
    """
    t=()
    for i in range(x,y+1):
        a=str(i)
        if a[::-1]==a:
            t+=(i,)
    return t

if __name__=="__main__":
    l=int(input("enter the lower limit"))
    u=int(input("enter the upper limit"))
    print(palindrome(l,u))
            

        
    
