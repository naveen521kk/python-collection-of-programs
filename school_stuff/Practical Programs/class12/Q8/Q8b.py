"""returns a list of all special numbers in the tuple.(EG.145=1!+4!+5!)
"""
def factorial(n):
    """This function calculates the factorial of value ``n`` passed."""
    if n == 1:
        return 1
    else:
        return n * factorial(n - 1)

def spl_no(l,u):
    """This function returns a tuple of special numbers in range specified
    """
    t=()
    sum1=0
    for i in range(l,u+1):
        org=i
        for k in range(len(str(i))):
            n=i%10
            f=factorial(n)
            sum1+=f
            i=i//10
        if org==sum1:
            t+=(org,)
        sum1=0
    print(t)
l=int(input("enter the lower limit"))
u=int(input("enter the upper limit"))
spl_no(l,u)
            
        
