"""Write a prg.using 2 udf(user defined functions) to print the 
sum of series x/2!-x3/4!+x5/6!-.......x 2n-1/2n!. 
"""
def factorial(n):
    """This function calculates the factorial of value ``n``
    passed.
    """
    if n == 1:
        return 1
    else:
        return n * factorial(n - 1)

def calculate_sum(x, n):
    """This function calculates the value of the series 
    x/2!-x3/4!+x5/6!-.......x 2n-1/2n!.
    where x, n are the valuse are ``x`` and number of terms 
    in x/2!-x3/4!+x5/6!-.......x 2n-1/2n!.
    """
    tsum = 0
    for i in range(1, n + 1):
        if i % 2:
            tsum += (x * (2 * i - 1)) / factorial(2 * i)
        else:
            tsum -= (x * (2 * i - 1)) / factorial(2 * i)
    return tsum

if __name__ == "__main__":
    x = int(input("Enter x: "))
    n = int(input("Enter n: "))
    print(calculate_sum(x,n))