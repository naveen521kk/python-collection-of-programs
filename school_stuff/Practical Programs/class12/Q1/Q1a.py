"""Write a prg.using 2 udf(user defined functions) to print the
sum of series x/3!+x3/5!+-------x2n-1/2n+1!
"""
def factorial(n):
    """This function calculates the factorial of value ``n``
    passed.
    """
    if n == 1:
        return 1
    else:
        return n * factorial(n - 1)
def calculate_sum(x, n):
    """This function calculates the value of the series 
    x/3!+x3/5!+-------x2n-1/2n+1!
    where x, n are the values are ``x`` and number of terms 
    in x/3!+x3/5!+-------x2n-1/2n+1!.
    """
    tsum = 0
    for i in range(1, n + 1):
        tsum += (x * ((2 * i) - 1)) / factorial((2 * i) + 1)
    return tsum
if __name__ == "__main__":
    x = int(input("Enter x: "))
    n = int(input("Enter n: "))
    print(calculate_sum(x,n))
