"""Create nested dictionary for categories seniors/juniors/subjuniors
for each house(Bharathi/Tagore etc) scores.  
Display house with max score for each category"""

records = {
    "shivaji": {
        "seniors": "",
        "juniors": "",
        "subjuniors": "",
    },
    "pratap": {
        "seniors": "",
        "juniors": "",
        "subjuniors": "",
    },
    "bharathi": {
        "seniors": "",
        "juniors": "",
        "subjuniors": "",
    },
    "tagore": {
        "seniors": "",
        "juniors": "",
        "subjuniors": "",
    },
}

for house in dict(records):
    for subcat in records[house]:
        records[house][subcat] = int(input("Enter Score of '{}' in '{}' house:".format(subcat, house)))
    print()
# show maxmimum score in a catagory
catagory = []
for house in records:
    for subcat in records[house]:
        catagory.append(subcat)
    break


for cats in catagory:
    max_score = 0
    max_scrore_info = ""
    for house in records:
        if records[house][cats] > max_score:
            max_score = records[house][cats]
            max_scrore_info = house
    print(
        "Maximum Score in {cats} is {max_score} by {max_scrore_info}".format(
            cats=cats, max_score=max_score, max_scrore_info=max_scrore_info
        )
    )
