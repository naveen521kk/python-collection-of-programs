"""Store subscriber name & phone in dictionary.  Perform as menu 
driven code & UDFs add, view, modify, delete a subscriber 
"""

mainDict = {}


def add_subscriber(name, phone):
    """This function adds a new subscriber in the dictionary

    Parameters
    ----------
    name : :class:`str`
        The user's name.
    phone : :class:`str`
        The user's phone number
    """
    mainDict[int(phone)] = name


def view_subscribers():
    """This function prints all the subscribers stored in dictionart"""
    mainDict_keys = list(mainDict.keys())
    print("List of subscribers are:")
    for i in range(len(mainDict_keys)):
        print("{}. {}\t-\t{}".format(i+1, mainDict[mainDict_keys[i]], mainDict_keys[i]))


def modify_subscriber(changed, phone=None, new_name=None, new_ph=None):
    """This function modifies the user in the dictionary using the phone number.

    Parameters
    ----------
    changed : :class:`str`
        What is changed? Either ``phone`` or ``name``.
    phone : :class:`str`
        The user's phone number phone number if name is changed.
        The user's old phone number when phone number is changed
    new_name : :class:`str`
        The user's changed name.
    new_ph : :class:`str`
        The new Phone number if phone number is changed.

    """
    if changed == "name":
        if phone:
            mainDict[int(phone)] = new_name
        else:
            print("Need a valid phone number")
    elif changed == "phone":
        old_ph = phone
        if old_ph:
            name = mainDict[int(old_ph)]
            delete_subscriber(int(old_ph))
            add_subscriber(name, int(new_ph))
    else:
        raise NotImplementedError("Only name or phone is allowed")


def delete_subscriber(phone):
    """This function modifies the user in the dictionary using the phone number.

    Parameters
    ----------
    phone : :class:`str`
        The user's phone number

    Returns
    -------
    :class:`str`
        The name of subscriber
    """
    return mainDict.pop(int(phone), "Looks like the subscriber doesn't exists")


while True:
    print(
        "What do you want to do?",
        "1. Add a new Subscriber",
        "2. View all Subscribers",
        "3. Modify Existing Subscribers",
        "4. Delete a Subscriber",
        "0. Exit",
        sep="\n",
    )
    chk = int(input())
    if chk == 0:
        print("Program Over.")
        break
    else:
        if chk == 1:
            subs_name = input("Enter Subscriber name:")
            phone_no = input("Enter Subscriber Phone Number:")
            add_subscriber(subs_name, phone_no)
            print("Sucessfully Added.")
        elif chk == 2:
            view_subscribers()
        elif chk == 3:
            while True:
                print(
                    "Modifying User info:",
                    "What do you need to do?",
                    "1.Edit Name",
                    "2.Edit Phone Number:",
                    "0. Exit",
                    sep="\n",
                )
                inp = int(input())
                if inp == 1:
                    phone_no = input("Enter subscriber's phone number:")
                    mod_name = input("Enter the new name of subscriber:")
                    modify_subscriber(changed="name", phone=phone_no, new_name=mod_name)
                    break
                elif inp == 2:
                    phone_no = input("Enter subscriber's new phone number:")
                    old_ph = input("Enter old ph no:")
                    modify_subscriber("phone", phone=old_ph, new_ph=phone_no)
                    break
                elif inp==0:
                    break
        elif chk==4:
            phone = input("Enter subscriber's phone number")
            delete_subscriber(phone)
        else:
            continue
        print("DO you want to continue?[Y/n]",end="")
        res=input()
        if res.lower()=='n':
            break
        else:
            continue