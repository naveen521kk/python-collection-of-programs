"""WAP to generate n different integers between x and y
and find the largest,second largest and prime nos(USING RANDOM)."""


def isprime(n):
    """Checks whether ``n`` is a prime number"""
    isprime = ""
    if n == 2:
        isprime = "Is prime."
    else:
        for j in range(2, n):
            if n % j == 0:
                isprime = ""
                break
            else:
                isprime = "Is prime."
    if isprime:
        return True
    else:
        return False


def sort(integers):
    """Sorts the list ``integers`` using bubble sort technique"""
    n = len(integers)
    for i in range(n):
        for j in range(0, n - i - 1):
            if integers[j] < integers[j + 1]:
                integers[j], integers[j + 1] = integers[j + 1], integers[j]
    return integers


import random

upper_limit = int(input("enter the upper limit"))
lower_limit = int(input("enter the lower limit"))
n = int(input("enter the no.of.integers"))
numbers_list = []
while True:
    if n > (upper_limit - lower_limit):
        raise Exception("Number of elements is more that limits")
    a = random.randint(lower_limit, upper_limit)
    if a not in numbers_list:
        numbers_list.append(a)
        print(a)
    if len(numbers_list) == n:
        break
print(numbers_list)
numbers_list = sort(numbers_list)
print("Smallest=", numbers_list[0])
print("Largest=", numbers_list[-1])
for i in numbers_list:
    if isprime(i):
        print(i, "is a prime no")
