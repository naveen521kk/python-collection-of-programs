"""
Sorting of elements row wise and column wise using 
i.bubble sort
ii. Selection sort.
"""


def bubble_sort(lst):
    """Takes a list and sorts using
    bubble sort algorithm
    ascending
    """
    n = len(lst)
    for i in range(n):
        for j in range(0, n-i-1):
            if lst[j] > lst[j+1]:
                lst[j], lst[j+1] = lst[j+1], lst[j]
    return lst


def insertion_sort(lst):
    """Takes a list and sorts using
    insertion sort algorithm
    ascending
    """
    sorted = list(lst)
    for i in range(1, len(sorted)):
        key = sorted[i]
        j = i-1
        while j >= 0 and key < sorted[j]:
            sorted[j+1] = sorted[j]
            j = j-1
        else:
            sorted[j+1] = key
    return sorted

def sort_matrix(row_or_column,matrix,sorting_method):
    """Sort matrix row or column wise.
    if choice is `row` sorted row wise
    else column wise

    sorting_method -> bubble or insertion
    default insertion
    """
    if row_or_column == "row":
        for row_n in range(len(matrix)):
            if sorting_method == "bubble":
                matrix[row_n] = bubble_sort(matrix[row_n])
            else:
                matrix[row_n] = insertion_sort(matrix[row_n])
    else:
        # first change row column to column row
        new = []
        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                if len(new) <= j:
                    new.append([])
                new[j].append(matrix[i][j])
        matrix = new
        for row_n in range(len(matrix)):
            if sorting_method == "bubble":
                matrix[row_n] = bubble_sort(matrix[row_n])
            else:
                matrix[row_n] = insertion_sort(matrix[row_n])
        final = []
        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                if len(final) <= j:
                    final.append([])
                final[j].append(matrix[i][j])
        return final
if __name__ == "__main__":
    print(sort_matrix('column',[[1,2,3],[3,2,1],[4,5,6]],'i'))