"""
Generate library functions :
a.Left and  right diagonal swapping.
"""


def swap_diagonals(matrix):
    """Swaps the diagonal element of a matrix given 
    by nested list. 

    Parameters
    ----------

    matrix : :class:`list`
        The matrix.
    Note
    ----
    The Matrix must be a Square Matrix for this function to Work.

    Example
    -------

    >>> swap_diagonals([[1,2,3],[2,4,6],[5,6,9]])
    [[3,2,1],[2,4,6],[9,6,5]]

    >>> swap_diagonals([['a','b'],[1,2]])
    [['b','a'],[2,1]]

    >>> swap_diagonals([[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]])
    [[4,2,3,1],[5,7,6,8],[9,11,10,12],[16,14,15,13]]

    """
    new_matrix = list(matrix)  # for a true copy
    for row_n in range(len(matrix)):
        for column_n in range(1, len(matrix[row_n])+1):
            if row_n == column_n-1:
                temp = new_matrix[row_n][column_n-1]
                new_matrix[row_n][column_n-1] = new_matrix[row_n][-column_n]
                new_matrix[row_n][-column_n] = temp
    return new_matrix


if __name__ == "__main__":
    print(swap_diagonals([[1, 2, 3], [2, 4, 6], [5, 6, 9]]))
    print(swap_diagonals([[1, 2, 3, 4], [5, 6, 7, 8],
                          [9, 10, 11, 12], [13, 14, 15, 16]]))
    print(swap_diagonals([['a', 'b'], [1, 2]]))
