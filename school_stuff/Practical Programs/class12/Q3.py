"""WAP using menu to create text file
a.Check no of lines.
b.no of lines containing ‘U’ and write the lines onto another file
c.To convert upper to lowercase and viceversa. 
"""


def filewrite():
    """This function is used for taking input in multi lines.
    """
    a = input()
    if a:
        return a + "\n" + filewrite()
    else:
        return ""


while True:
    print(
        "File Utility:",
        "All files are read from Path asked.",
        "1. Check no of lines.",
        "2. no of lines containing ‘U’ and write the lines onto another file",
        "3. To convert upper to lowercase and viceversa.",
        "4. Write to a file",
        "0. Exit",
        sep="\n",
        end="\n",
    )
    cho = int(input("Enter Response:"))
    if cho == 0:
        print("Program Completed")
        break
    else:
        path = input("Enter File Path(relative or absolute):")
        if cho == 1:
            with open(path, "r") as f:
                nlines = len(f.readlines())
            print("No of lines is: %s" % nlines)
        elif cho == 2:
            with open(path, "r") as f:
                lines = f.readlines()
                tempLst = []
                for i in lines:
                    if "U" in i:
                        tempLst.append(i)
            print("No of lines containing 'U'", len(tempLst))
            path1 = input("Enter the file to write lines containing 'U':")
            with open(path1, "w") as f:
                f.writelines(tempLst)
            print("Wrote content into %s" % path1)
        elif cho == 3:
            with open(path, "r") as f:
                content = f.read()
            temp = ""
            for i in content:
                if i.isalpha():
                    if i.isupper():
                        temp += i.lower()
                    elif i.islower():
                        temp += i.upper()
            print("The switched test is: \n%s" % temp)
        elif cho == 4:
            print("Enter file contents below")
            content = filewrite()
            with open(path, "w") as f:
                f.write(content)
            print("Written sucessfully")
        else:
            print("Invalid response")
    print("Do you want to continue [Y/n]")
    if input().lower() == "n":
        break
